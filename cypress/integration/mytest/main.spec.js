describe("obtener denuncias", () => {
  it("Visitando la pagina", () => {
    cy.visit(
      "https://www.gestiondefiscalias.gob.ec/siaf/informacion/web/noticiasdelito/index.php"
    );

    cy.get("#pwd").type("0804539104");

    cy.get('input[value="Buscar Denuncia"]').click();

    cy.get("#resultados").children();

    // Cypress.Commands.add("clickRecaptcha", () => {
    //   cy.window().then((win) => {
    //     win.document
    //       .querySelector("iframe[src*='recaptcha']")
    //       .contentDocument.getElementById("recaptcha-token")
    //       .click();
    //   });
	// });
	
  });
});

// describe('Obtener Cedula',()=>{
// 	it("Visitando la pagina del registro civil",()=>{
// 		cy.visit("https://servicios.registrocivil.gob.ec/cdd/");

// 		cy.contains("NOMBRE").click();

// 		cy.get('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel2_TextBoxPrimerApellido').type("huerlo");

// 		cy.get('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel2_TextBoxSegundoApellido')//.type("quintero");

// 		cy.get('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel2_TextBoxPrimerNombre')//.type("jurgen");

// 		cy.get('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel2_TextBoxFechaNacimiento').type("/01 04 1998");

// 		cy.get('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel2_ButtonConsultarXNombre').click();

// 	})
// })

describe('ingreso',()=>{
    it("ir a pagina",()=>{
		cy.visit("http://certificados.ministeriodegobierno.gob.ec/gestorcertificados/antecedentes/");
        cy.get('.ui-dialog-buttonpane > :nth-child(2) > .ui-button-text').click();
	})

    it("ingreso de CI", ()=>{
		cy.get('#txtCi').type("0850251182");
		cy.get('#btnSig1 > .ui-button-text').click();
	})

    it("Motivo", ()=>{
		cy.get('#txtMotivo').type("Documentación para curriculum vitae");
        cy.get('#btnSig2 > .ui-button-text').click();

	})

    it("Visualizar documento", ()=>{
		cy.get('#btnOpen > .ui-button-text').click();
	})
})